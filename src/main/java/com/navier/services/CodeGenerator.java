package com.navier.services;

import java.util.ArrayList;
import java.util.Arrays;

public class CodeGenerator {
    /**
     * Use these 32 words as radix
     * 0123456789abcdefghijklmnopqrstuv
     * <p>
     * replace the possible confuse words as wxyz
     * 0 -> w
     * 1 -> x
     * i -> y
     * o -> z
     */
    public static final long MAX = 33554432; // 32 ^ 5
    public static final long P = 15485077;
    public static final long Q = 9007;
    public static final int LEN = 5;
    public static final int RADIX = 32;

    public static ArrayList<String> create(String type, int count, int firstIndex) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = firstIndex; i < firstIndex + count; i++) {
            if (type.equalsIgnoreCase("DartRays")) {
                list.add("B" + encode(i));
            } else {
                list.add(encode(i));
            }
        }
        return list;
    }

    private static String encode(int number) {
        if (number <= 0 || number > MAX) {
            throw new IllegalArgumentException();
        }
        long x = ((long) number * P + Q) % MAX;
        char[] codes = new char[LEN];
        Arrays.fill(codes, '0');
        String str = Long.toString(x, RADIX);
        System.arraycopy(str.toCharArray(),
                0,
                codes,
                LEN - str.length(),
                str.length());
        reverse(codes);
        replaceConfusableCode(codes);
        return new String(codes).toUpperCase();
    }

    /**
     * i,o,1,0 are possible confuse characters, we replace it by w,x,y,z
     */
    private static void replaceConfusableCode(char[] codes) {
        for (int i = 0; i < LEN; i++) {
            if(codes[i] == '0')codes[i] = 'w';
            if(codes[i] == '1')codes[i] = 'x';
            if(codes[i] == 'i')codes[i] = 'y';
            if(codes[i] == 'o')codes[i] = 'z';
        }
    }

    private static void reverse(char[] codes) {
        for (int i = LEN >> 1; i-- > 0; ) {
            codes[i] ^= codes[LEN - i - 1];
            codes[LEN - i - 1] ^= codes[i];
            codes[i] ^= codes[LEN - i - 1];
        }
    }
}
