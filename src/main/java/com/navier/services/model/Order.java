package com.navier.services.model;

import com.google.cloud.firestore.annotation.ServerTimestamp;
import com.navier.services.constant.OrderState;

import java.util.Date;

public class Order {

    /**
     * Creation timestamp
     */
    @ServerTimestamp
    Date timestamp;

    /**
     * The category
     */
    String category;

    /**
     * State of the order
     */
    String state = OrderState.CREATED;

    /**
     * The count of codes in this order
     */
    int count = 0;

    /**
     * The meta data
     */
    String meta;

    public Order(){

    }

    public Order(String state, String category, int count){
        this.state = state;
        this.category = category;
        this.count = count;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }
}
