package com.navier.services.model;

import java.util.List;

public class CodeList {
    List<String> codes;
    public CodeList(){}

    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }
}
