package com.navier.services.model;

import com.navier.services.constant.NavierCodeState;

public class NavierCode {

    /**
     * Order id
     */
    String orderid;

    /**
     * Account bound with user
     */
    String account;

    /**
     * Bind state
     */
    String state = NavierCodeState.UNBOUND;

    /**
     * The code that same as the id
     */
    String code;
    /**
     * A must have empty constructor
     */
    public NavierCode(){

    }

    public NavierCode(String orderid, String code){
        this.orderid = orderid;
        this.code = code;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
