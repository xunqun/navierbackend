package com.navier.services.constant;

public class OrderState {
    public static String CREATED = "created";
    public static String ACTIVATED = "activated";
    public static String DISABLED = "disabled";
}
