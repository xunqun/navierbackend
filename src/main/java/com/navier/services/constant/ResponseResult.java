package com.navier.services.constant;

public class ResponseResult {
    public static final String OK = "ok";
    public static final String FAIL = "fail";
    public static final String ERROR = "error";

}
