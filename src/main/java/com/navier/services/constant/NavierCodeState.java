package com.navier.services.constant;

public class NavierCodeState {
    public static String UNBOUND = "unbound";
    public static String BOUND = "bound";
    public static String DISABLED = "disabled";
    public static String ERROR_NO_SUCH_ITEM = "error_no_such_item";
    public static String ERROR_DUPLICATE_BINDING = "error_duplicate_binding";
    public static String ERROR_CODE_DISABLED = " error_code_disabled";
}
