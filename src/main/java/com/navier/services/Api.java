/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navier.services;


import com.google.api.core.ApiFuture;
import com.google.api.server.spi.config.*;
import com.google.cloud.firestore.*;
import com.navier.services.constant.NavierCodeState;
import com.navier.services.constant.OrderState;
import com.navier.services.constant.ResponseResult;
import com.navier.services.model.Message;
import com.navier.services.model.NavierCode;
import com.navier.services.model.Order;
import com.google.auth.oauth2.GoogleCredentials;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.navier.services.response.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.google.firebase.cloud.FirestoreClient.getFirestore;

/**
 * Api skeleton endpoints sample
 * Add your first API methods in this class, or you may create another class.
 * In that case, update the src/main/webapp/WEB-INF/web.xml and modify
 * the class set to the services param as a comma separated list.
 * <p>
 * For example:
 * <init-param>
 * <param-name>services</param-name>
 * <param-value>com.example.skeleton.FirstApi, com.example.skeleton.SecondApi</param-value>
 * </init-param>
 */
@com.google.api.server.spi.config.Api(name = "navierkey",
        version = "v2",
        namespace =
        @ApiNamespace(
                ownerDomain = "navierhud2017.appspot.com",
                ownerName = "navierhud2017.appspot.com",
                packagePath = ""
        )

)
public class Api {
    private Firestore db;
    private static final String projectId = "navierhud2017";
    private static final String CollectionOrder = "order";
    private static final String CollectionNavierCode = "naviercode";

    private CreateOrderResponse createOrderResponse;

    @ApiMethod(name = "echo")
    public Message echo(Message message, @Named("n") @Nullable Integer n) {
        return doEcho(message, n);
    }

    private Message doEcho(Message message, Integer n) {
        if (n != null && n >= 0) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < n; i++) {
                if (i > 0) {
                    sb.append(" ");
                }
                sb.append(message.getMessage());
            }
            message.setMessage(sb.toString());
        }
        return message;
    }

    @ApiMethod(name = "createorder")
    public CreateOrderResponse createOrder(@Named("n") Integer n, @Named("type") String type, @Named("meta") @Nullable String meta) {
        return doCreateOrder(n, type, meta);
    }

    @ApiMethod(name = "retrieveorder")
    public RetrieveOrderResponse retrieveOrder(@Named("id") String id) {
        return doRetrieveOrder(id);
    }


    @ApiMethod(name = "activateorder")
    public ActivateOrderResponse activateOrder(@Named("orderid") String orderId) {
        return doActivateOrder(orderId);
    }

    @ApiMethod(name = "disableorder")
    public DisableOrderResponse disableOrder(@Named("orderid") String orderId) {
        return doDisableOrder(orderId);
    }

    @ApiMethod(name = "disablecode")
    public DisableCodeResponse disableCode(@Named("code") String code) {
        return doDisableCode(code);
    }


    @ApiMethod(name = "codevalidate")
    public CodeValidateResponse codeValidate(@Named("code") String code) {
        return doCodeValidate(code);
    }

    @ApiMethod(name = "bindaccount")
    public BindAccountResponse bindAccount(@Named("code") String code, @Named("account") String account) {
        return doBindAccount(code, account);
    }


    private RetrieveOrderResponse doRetrieveOrder(String id) {
        RetrieveOrderResponse response = new RetrieveOrderResponse();
        response.setOrderid(id);
        try {
            ApiFuture<DocumentSnapshot> orderFuture = db.collection(CollectionOrder).document(id).get();
            ApiFuture<QuerySnapshot> future =
                    db.collection(CollectionNavierCode).whereEqualTo("orderid", id).get();

            if (!orderFuture.get().exists() || future == null) {
                response.setResult(ResponseResult.FAIL);
                return response;
            }
            List<QueryDocumentSnapshot> documents = future.get().getDocuments();
            List<String> result = new ArrayList<>();
            for (DocumentSnapshot document : documents) {
                result.add(document.getId());
            }

            response.setOrder(orderFuture.get().toObject(Order.class));
            response.setCode(result);
            response.setResult(ResponseResult.OK);
        } catch (Exception e) {
            e.printStackTrace();
            response.setResult(ResponseResult.FAIL);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    private DisableCodeResponse doDisableCode(String code) {
        DisableCodeResponse response = new DisableCodeResponse();
        try {
            setupFirestore();

            DocumentReference docRef = db.collection(CollectionNavierCode).document(code);
            docRef.update("state", NavierCodeState.DISABLED);
            response.setCode(docRef.get().get().toObject(NavierCode.class));
            response.setResult(ResponseResult.OK);
        } catch (Exception e) {
            response.setResult(ResponseResult.ERROR);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    private BindAccountResponse doBindAccount(String code, String account) {
        BindAccountResponse response = new BindAccountResponse();
        response.setAccount(account);
        response.setCode(code);
        try {
            setupFirestore();

            DocumentReference docRef = db.collection(CollectionNavierCode).document(code);
            DocumentSnapshot document = docRef.get().get();
            NavierCode navierCode;
            if (document.exists()) {
                navierCode = document.toObject(NavierCode.class);
                if (navierCode.getState().equals("disabled")) {
                    response.setState(NavierCodeState.ERROR_CODE_DISABLED);
                    response.setResult(ResponseResult.FAIL);
                } else if (navierCode.getAccount() == null) {
                    navierCode.setAccount(account);
                    navierCode.setState(NavierCodeState.BOUND);
                    docRef.set(navierCode);
                    response.setState(NavierCodeState.BOUND);
                    response.setResult(ResponseResult.OK);
                } else {
                    response.setState(NavierCodeState.ERROR_DUPLICATE_BINDING);
                    response.setResult(ResponseResult.FAIL);
                }
            } else {
                response.setState(NavierCodeState.ERROR_NO_SUCH_ITEM);
                response.setResult(ResponseResult.FAIL);
            }

        } catch (Exception e) {
            response.setResult(ResponseResult.ERROR);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    private CodeValidateResponse doCodeValidate(String code) {
        CodeValidateResponse response = new CodeValidateResponse();
        try {
            setupFirestore();
            DocumentReference docRef = db.collection(CollectionNavierCode).document(code);
            DocumentSnapshot document = docRef.get().get();
            NavierCode navierCode;
            if (document.exists()) {
                navierCode = document.toObject(NavierCode.class);
                response.setCode(navierCode);
                response.setResult(ResponseResult.OK);
            } else {
                response.setState(NavierCodeState.ERROR_NO_SUCH_ITEM);
                response.setResult(ResponseResult.FAIL);
            }

        } catch (Exception e) {
            response.setResult(ResponseResult.ERROR);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    private DisableOrderResponse doDisableOrder(String orderId) {
        DisableOrderResponse response = new DisableOrderResponse();
        try {
            setupFirestore();
            WriteBatch batch = db.batch();
            DocumentReference docRef = db.collection(CollectionOrder).document(orderId);
            batch.update(docRef, "state", OrderState.DISABLED);

            CollectionReference colRef = db.collection(CollectionNavierCode);
            Query query = colRef.whereEqualTo("orderid", orderId);
            for (QueryDocumentSnapshot document : query.get().get().getDocuments()) {
                batch.update(document.getReference(), "state", NavierCodeState.DISABLED);
            }

            batch.commit();
            response.setResult(ResponseResult.OK);
        } catch (Exception e) {
            response.setResult(ResponseResult.ERROR);
            response.setMessage(e.getMessage());
        }
        return response;
    }


    private ActivateOrderResponse doActivateOrder(String orderId) {
        ActivateOrderResponse response = new ActivateOrderResponse();
        try {
            setupFirestore();
            WriteBatch batch = db.batch();

            DocumentReference docRef = db.collection(CollectionOrder).document(orderId);
            batch.update(docRef, "state", OrderState.ACTIVATED);

            updateCodeState(batch, orderId);
            batch.commit();
            response.setResult(ResponseResult.OK);
            response.setMessage("");
        } catch (Exception e) {
            response.setResult(ResponseResult.ERROR);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    private void updateCodeState(WriteBatch batch, String orderId) throws ExecutionException, InterruptedException {
        ApiFuture<QuerySnapshot> future =
                db.collection(CollectionNavierCode).whereEqualTo("orderid", orderId).get();

        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        for (QueryDocumentSnapshot snapshot : documents) {
            DocumentReference docRef = snapshot.getReference();
            NavierCode navierCode = snapshot.toObject(NavierCode.class);
            if (navierCode.getAccount() != null && navierCode.getAccount().length() > 0) {
                batch.update(docRef, "state", NavierCodeState.BOUND);
            } else {
                batch.update(docRef, "state", NavierCodeState.UNBOUND);
            }
        }

    }

    private CreateOrderResponse doCreateOrder(int count, String type, String meta) {
        // Use the application default credentials

        try {
            createOrderResponse = new CreateOrderResponse();
            if (count <= 0 || count > 490) {
                createOrderResponse.setResult(ResponseResult.FAIL);
                createOrderResponse.setMessage("Invalid count number. It should be more than 0 and less than 490");
                return createOrderResponse;
            }

            setupFirestore();
            WriteBatch batch = db.batch();

            String orderId = insertOrder(batch, count, type, meta);
            insertCode(type, batch, count, orderId);
            batch.commit();
            createOrderResponse.setResult(ResponseResult.OK);
        } catch (Exception e) {
            e.printStackTrace();
            createOrderResponse.setResult(ResponseResult.ERROR);
            createOrderResponse.setMessage(e.getMessage());
        }

        return createOrderResponse;
    }

    private String insertOrder(WriteBatch batch, int count, String type, String meta) throws ExecutionException, InterruptedException {
        Order order = new Order(OrderState.CREATED, type, count);
        order.setMeta(meta);

        int size = db.collection(CollectionOrder).get().get().getDocuments().size();
        DocumentReference docRef = db.collection(CollectionOrder).document(CodeGenerator.create("orderid", 1, size + 1).get(0));
        batch.set(docRef, order);

        createOrderResponse.setOrder(order);
        createOrderResponse.setOrderid(docRef.getId());
        return docRef.getId();
    }

    private void insertCode(String type, WriteBatch batch, int count, String orderId) throws Exception {
        int size = db.collection(CollectionNavierCode).get().get().getDocuments().size();
        if (size + count < CodeGenerator.MAX) {
            List<String> list = CodeGenerator.create(type, count, size + 1);
            for (String s : list) {
                NavierCode code = new NavierCode(orderId, s);
                code.setState(NavierCodeState.UNBOUND);
                batch.set(db.collection(CollectionNavierCode).document(s), code);
            }
            if (count > 1) {
                createOrderResponse.setRange("from " + (size + 1) + " to " + (size + count));
            } else {
                createOrderResponse.setRange(" " + (size + count));
            }
            createOrderResponse.setCode(list);
        } else {
            throw new IndexOutOfBoundsException("The size of code has exceed the max of generation limit");
        }
    }

    private void setupFirestore() throws IOException {
        if (db == null) {
            GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(credentials)
                    .setProjectId(projectId)
                    .build();
            FirebaseApp.initializeApp(options);

            db = getFirestore();
        }
    }
}

