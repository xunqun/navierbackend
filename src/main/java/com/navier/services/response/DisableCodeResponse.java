package com.navier.services.response;


import com.navier.services.model.NavierCode;

public class DisableCodeResponse extends BaseResponse {
    NavierCode code;

    public DisableCodeResponse() {

    }

    public NavierCode getCode() {
        return code;
    }

    public void setCode(NavierCode code) {
        this.code = code;
    }
}
