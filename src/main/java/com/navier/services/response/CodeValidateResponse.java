package com.navier.services.response;


import com.navier.services.model.NavierCode;

public class CodeValidateResponse extends BaseResponse {
    String state;
    NavierCode code;
    public CodeValidateResponse() {

    }

    public NavierCode getCode() {
        return code;
    }

    public void setCode(NavierCode code) {
        this.code = code;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
