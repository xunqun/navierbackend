package com.navier.services.response;

import com.navier.services.constant.ResponseResult;
import com.navier.services.model.NavierCode;
import com.navier.services.model.Order;

import java.util.List;

public class CreateOrderResponse extends BaseResponse {
    String orderid;
    Order order;
    String range;
    List<String> code;

    public CreateOrderResponse() {

    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public List<String> getCode() {
        return code;
    }

    public void setCode(List<String> code) {
        this.code = code;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }
}
