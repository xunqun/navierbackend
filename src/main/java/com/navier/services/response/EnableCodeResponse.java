package com.navier.services.response;


import com.navier.services.model.NavierCode;

public class EnableCodeResponse extends BaseResponse {
    NavierCode code;

    public EnableCodeResponse() {

    }

    public NavierCode getCode() {
        return code;
    }

    public void setCode(NavierCode code) {
        this.code = code;
    }
}
