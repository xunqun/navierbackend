package com.navier.services.response;


import com.navier.services.model.Order;

import java.util.List;

public class RetrieveOrderResponse extends BaseResponse {
    String orderid;
    Order order;
    List<String> code;

    public RetrieveOrderResponse() {

    }

    public List<String> getCode() {
        return code;
    }

    public void setCode(List<String> code) {
        this.code = code;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
