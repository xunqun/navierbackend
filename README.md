## Setup endpoint enviroment

1. Please refer to the link at cloud.google.com / [setting up a developement environment](https://cloud.google.com/endpoints/docs/frameworks/java/set-up-environment)
2. Open project in the IntelliJ IDEA and setup the gradle path like this (/Applications/Android Studio.app/Contents/gradle/gradle-4.1), you can found the exactly path via the following command

        ls -d "/Applications/Android Studio.app/Contents/gradle"/*
        
## Setup firebase enviroment

1. Refer to the link at firebase.google.com / [Get started with Cloud Firestore](https://firebase.google.com/docs/firestore/quickstart)


## Project deployment

- Refer to the link at cloud.google.com / [Deploying and Testing an API](https://cloud.google.com/endpoints/docs/frameworks/java/test-deploy)
- Google cloud login and settings
    * Make sure that Cloud SDK is authorized to access your data and services on Google Cloud Platform:
    
            gcloud auth login
            
    * Use Application Default Credentials:
            
            gcloud auth application-default login
              
    * Install the Cloud SDK app-engine-java component
            
            gcloud components install app-engine-java
            
    * Set the default project to your project ID.
    
            gcloud config set project [YOUR_PROJECT_ID]
    
- Generating and deploying an API configuration
    * Generate an OpenAPI document using the framework tools, for example:
        
            ./gradlew endpointsOpenApiDocs
    
    * Deploy the resulting configuration .json file, for example:
        
            gcloud endpoints services deploy build/endpointsOpenApiDocs/openapi.json
- Used the following command to deploy application to remote

        ./gradlew appengineDeploy

## Result pages
1. [Developer's Apis document](https://endpointsportal.navierhud2017.cloud.goog/docs/navierhud2017.appspot.com/1/introduction)
2. [Firebase database](https://console.firebase.google.com/u/0/project/navierhud2017/database/firestore/data)

## Troble shoting
1. 當library import 找不到時候，隨便在build.gradle 修改個library的版本再改回來，然後重啟